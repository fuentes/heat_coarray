set(CTEST_PROJECT_NAME "heat equation using coarrays fortran")
set(CTEST_NIGHTLY_START_TIME "00:00:00 GMT")
set(CTEST_DROP_METHOD "https")
set(CTEST_DROP_SITE "my.cdash.org")
set(CTEST_DROP_LOCATION "/submit.php?project=heat_coarray")
set(CTEST_DROP_SITE_CDASH TRUE)
#--------------------------------------------------------------------
# Start with the short system name, e.g. "Linux", "FreeBSD" or "Windows"
set(BUILDNAME "${CMAKE_SYSTEM_NAME}")
# Add i386 or amd64
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(BUILDNAME "${BUILDNAME}-amd64")
else()
    set(BUILDNAME "${BUILDNAME}-i386")
endif()
# Add compiler name
get_filename_component(CMAKE_C_COMPILER_NAME ${CMAKE_Fortran_COMPILER} NAME)
set(BUILDNAME "${BUILDNAME}-${CMAKE_C_COMPILER_NAME}")
# Add the build type, e.g. "Debug, Release..."
if(CMAKE_BUILD_TYPE)
    set(BUILDNAME "${BUILDNAME}-${CMAKE_BUILD_TYPE}")
endif(CMAKE_BUILD_TYPE)
