module collective_tests

    use constants
    use asserts
    use parallel
    use collectives

    implicit none
    integer :: n = 12, s(2), me, i
    real(dp) :: prec = real(1.d-12, dp)
    real(dp) :: val, v, r
    real(dp), allocatable :: vec(:), res(:)
    integer, allocatable :: vec_int(:), res_int(:)
    

contains   

   subroutine test_gather_to_root
       me = this_image()
       if (me  == 1) then
           vec = [1, 2, 3] * 1.0_dp
       else if ( me == 2) then
           vec  = [4, 5] * 1.0_dp
       else
           vec = [real(dp)::]
       end if
       res = gather_to_root(vec)
       if (me == 1) then
           call assert_equals(res, [1, 2, 3, 4, 5] * 1.0_dp, prec)
       end if
   end subroutine test_gather_to_root

   subroutine test_gather_to_root_integer
       select case (this_image())
       case (1) 
           vec_int = [1, 2, 3] 
       case (2) 
           vec_int  = [4, 5] 
       case (4)
           vec_int = [6]
       case (3)
           vec_int = [integer::]
       end select
       res_int = gather_to_root(vec_int)
       if (me == 1) then
           call assert_equals(res_int, [(i,i=1,6)])
       end if
   end subroutine test_gather_to_root_integer

   subroutine test_gather_to_all
       me = this_image()
       if (me  == 1) then
           vec = [1, 2, 3] * 1.0_dp
       else if ( me == 2) then
           vec  = [4, 5] * 1.0_dp
       else
           vec = [real(dp)::]
       end if
       res = gather_to_all(vec)
       call assert_equals(res, [1, 2, 3, 4, 5] * 1.0_dp, prec)
   end subroutine test_gather_to_all

   subroutine test_dot_product

       me = this_image()
       select case (me)
         case (1)
             vec = [1,2] * 1.0_dp
         case (2)
             vec = [3, 4] * 1.0_dp
         case (3)
             vec = [5, 6] * 1.0_dp
         case (4)
             vec = [7, 8] * 1.0_dp
         case  default
             vec = [real(dp)::]
         end select
         r = vec .dot. vec
         call assert_equals(r, sum([(i*i*1.0_dp, i=1,8)]), prec)
   end subroutine  test_dot_product
             

   subroutine test_broadcast

      real(dp), allocatable :: x(:)
      if (this_image() == 1) x = [1.,2.] * 1.0_dp
      call co_broadcast(x, source_image = 1)
      call assert_equals(x , [1., 2.] * 1.0_dp, prec)

   end subroutine test_broadcast

end module collective_tests
