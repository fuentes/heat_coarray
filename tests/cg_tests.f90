module cg_tests

    use constants
    use asserts
    use parallel
    use constants
    use collectives
    use csr_matrix_class
    use solver
    use io
    use tools

    implicit none
    integer :: n = 12, s(2), me, i
    real(dp) :: prec = real(1.d-12, dp)
    real(dp) :: val, v, r
    real(dp), allocatable :: x(:), b(:), check(:), mat_dense(:,:)
    integer, allocatable :: nrows(:)
    integer :: nrow(1)
    type(csr_matrix) :: mat 

contains   

    subroutine test_cg_simple
                                  
        mat = csr_matrix(8, 8, 14 + 8)
        me = this_image()

        select case(me)
        case (1)
            mat % row_ptr =  [ 1, 3, 6] 
            mat % col_val =  [ 1, 2, 1, 2, 3]
            mat % vals = [ 2, -1, -1, 2, -1] * 1.0_dp
            check = [13.333333333333332_dp, 25.666666666666664_dp]
        case (2)
            mat % row_ptr =  [ 1, 4, 7] 
            mat % col_val =  [ 2, 3, 4, 3, 4, 5] 
            mat % vals = pack(spread([-1,2,-1], 2, 2), .true.) * 1.0_dp
            check = [36.0_dp,43.333333333333336_dp]
        case (3)
            mat % row_ptr =  [ 1, 4, 7] 
            mat % col_val =  [ 4, 5, 6, 5, 6, 7]
            mat % vals = pack(spread([-1,2,-1], 2, 2), .true.) * 1.0_dp
            check = [46.66666666666668_dp, 45.0_dp]
        case (4)
            mat % row_ptr =  [ 1, 4, 6] 
            mat % col_val =  [ 6, 7, 8, 7, 8]
            mat % vals = [ -1, 2, -1, -1, 2] * 1.0_dp
            check=[37.33333333333333_dp, 22.666666666666664_dp]
        case default
            print *, "this test must  be run with 4 procs"
            stop -1
        end select
          
        nrow(1) = mat % local_nrows
        nrows = [0, cumsum(gather_to_all( nrow))]
        b = [(i, i=1+nrows(me),nrows(me+1))]*1.0_dp
        x = cg( mat, b , 100, 1.e-8_dp)
        call assert_equals(x, check, 1.0e-12_dp)
                                  
    end subroutine test_cg_simple

end module cg_tests
