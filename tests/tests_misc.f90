module tests_misc

    use iso_c_binding
    use asserts
    use heat
    use tools
    use constants
    implicit none
    real(kind=c_double) :: prec = real(1.d-12, dp)
    real(kind=dp), allocatable :: x(:), y(:)

contains   

   subroutine test_diff2_simple

       x = spread(1.d0 , 1, 4)
       y = [ -1.d0, 0.d0, 0.d0, -1.d0]
       call assert_equals_vec( diff2( x ), y, prec )

   end subroutine test_diff2_simple

   subroutine test_linspace

      x = linspace(0.d0, 1.d0, 5) 
      y = [0.d0, 0.25d0, 0.5d0, 0.75d0, 1.d0]
      call assert_equals_vec( x, y, prec )

   end subroutine test_linspace

end module tests_misc
