module csr_matrix_tests

    use constants
    use asserts
    use parallel
    use collectives
    use csr_matrix_class

    implicit none
    integer :: n, p, i
    real(kind=dp) :: prec = real(1.d-12, dp)
    real(kind=dp) :: val, v, r
    real(kind=dp), allocatable :: vec(:), res(:), check (:)
    type(csr_matrix)  :: mat

contains   

   !! test for sparse matrix vector with a non square matrix
   !!
   !!  A = [[ 1  0  0  0  0  0 ]
   !!       [ 0  2  0  0  0  0 ]
   !!       [ 0  0  3  4  0  0 ] 
   !!       [ 5  0  0  0  0  0 ]
   !!       [ 0  0  0  0  6  0 ]
   !!       [ 0  0  0  7  0  0 ]
   !!       [ 0  8  9  0  0  0 ]
   !!       [ 0 10 11  0  0  0]]

   subroutine test_spmv_simple
 
       mat = csr_matrix(8, 6, 11)
       vec = spread(1.0_dp, 1, 6) 
       res = spread(0.0_dp, 1, mat % local_nrows)

       select case(this_image())
       case (1)
           mat % row_ptr =  [ 1, 2, 3]
           mat % col_val =  [ 1, 2]
           mat % vals = [ 1, 2] * 1.0_dp
           check = [ 1, 2] * 1.0_dp
       case (2)
           mat % row_ptr =  [ 1, 3, 4]
           mat % col_val =  [ 3, 4, 1]
           mat % vals = [ 3, 4, 5] * 1.0_dp
           check = [ 7, 5] * 1.0_dp
       case (3)
           mat % row_ptr =  [ 1, 2, 3]
           mat % col_val =  [ 5, 4]
           mat % vals = [ 6, 7] * 1.0_dp
           check = [ 6, 7] * 1.0_dp
       case (4)
           mat % row_ptr =  [ 1, 3, 5]
           mat % col_val =  [ 2, 3, 2, 3]
           mat % vals = [ 8, 9, 10, 11] * 1.0_dp
           check = [ 17, 21] * 1.0_dp
       case default
           print *, "this test must be runned with 4 images"
           stop -1
       end select
         
       res = mat % mat_vec( vec )
       call assert_equals(res, check, prec)

   end subroutine test_spmv_simple 

end module csr_matrix_tests

