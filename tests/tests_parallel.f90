module tests_parallel

    use iso_c_binding
    use asserts
    use parallel
    use constants
    use heat
    implicit none
    integer :: n = 12, s(2)
    real(kind=dp) :: prec = real(1.d-12, dp)
    real(kind=dp), allocatable :: x(:)[:], y(:)[:]

contains   

   subroutine test_exchange

       s = tile( n )
       allocate( x(s(1)-1:s(2)+1)[*] )
       !print '(i0ai2ai2a)', this_image(), " => [",lbound(x),",",ubound(x),"]"
       sync all
       x(:) = [0.d0, spread(1.d0, 1, s(2)-s(1)+1), 0.d0]
       call halo_exchange_1d(n, x)
       print *, x
       call assert_equals_vec( diff2( x ), y, prec )

   end subroutine test_exchange

end module tests_parallel
