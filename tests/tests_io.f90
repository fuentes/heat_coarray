module tests_io

    use iso_c_binding
    use asserts
    use io
    use constants
    implicit none

contains   

    ! TODO  : do a true check !
   subroutine test_println_mat
       integer :: i 
       real(dp), allocatable ::  a(:,:)

       a = reshape([(i,i=1,6)]*1.0_dp,[3,2])
       call println(a)

   end subroutine test_println_mat

end module tests_io
