#Taken from https://github.com/modern-fortran/weather-buoys, some adaptations from marc fuentes
#MIT License
#
#Copyright (c) 2018 Milan Curcic
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

#FindCoarray
#----------
#
#Finds compiler flags or library necessary to support Fortran 2008/2018 coarrays.
#
#This packages primary purposes are:
#
#* for compilers natively supporting Fortran coarrays without needing compiler options, simply indicating Coarray_FOUND  (example: Cray)
#* for compilers with built-in Fortran coarray support, enable compiler option (example: Intel Fortran)
#* for compilers needing a library such as OpenCoarrays, presenting library (example: GNU)
#
#
#Result Variables
#^^^^^^^^^^^^^^^^
#
#``Coarray_FOUND``
#  indicates coarray support found (whether built-in or library)
#
#``Coarray_LIBRARIES``
#  coarray library path
#``Coarray_COMPILE_OPTIONS``
#  coarray compiler options
#``Coarray_EXECUTABLE``
#  coarray executable e.g. ``cafrun``
#``Coarray_MAX_NUMPROCS``
#  maximum number of parallel processes
#``Coarray_NUMPROC_FLAG``
#  use for executing in parallel: ${Coarray_EXECUTABLE} ${Coarray_NUMPROC_FLAG} ${Coarray_MAX_NUMPROCS} ${CMAKE_CURRENT_BINARY_DIR}/myprogram
#  
#``Coarray_shared`` : indicate if implementation of Coarray is shared or distributed
#Cache Variables
#^^^^^^^^^^^^^^^^
#
#The following cache variables may also be set:
#
#``Coarray_LIBRARY``
#  The coarray libraries, if needed and found
#]=======================================================================]

cmake_policy(VERSION 3.3)

set(options_coarray Intel NAG)  # flags needed
set(opencoarray_supported GNU)  # future: Flang, etc.

unset(Coarray_COMPILE_OPTIONS)
unset(Coarray_LIBRARY)
unset(Coarray_REQUIRED_VARS)

if(CMAKE_Fortran_COMPILER_ID IN_LIST options_coarray)

  if(CMAKE_Fortran_COMPILER_ID STREQUAL Intel)
    set(Coarray_COMPILE_OPTIONS -coarray=shared)
    set(Coarray_LIBRARY -coarray=shared)  # ifort requires it at build AND link
    set(Coarray_shared TRUE)
    set(Coarray_var FOR_COARRAY_NUM_IMAGES)
  endif()
  
  
  if(CMAKE_Fortran_COMPILER_ID STREQUAL NAG)
   set(Coarray_COMPILE_OPTIONS -coarray=cosmp)
   set(Coarray_LIBRARY -coarray=cosmp)  
   set(Coarray_shared TRUE)
   set(Coarray_var NAGFORTRAN_NUM_IMAGES)
  endif()
  
  list(APPEND Coarray_REQUIRED_VARS ${Coarray_LIBRARY})

elseif(CMAKE_Fortran_COMPILER_ID IN_LIST opencoarray_supported)

  find_package(OpenCoarrays)

  if(OpenCoarrays_FOUND)
    set(Coarray_LIBRARY OpenCoarrays::caf_mpi_static)
    message(STATUS "OpenCoarrays DIR = ${OpenCoarrays_DIR}")
    find_program(CAFRUN NAMES cafrun PATHS "${OpenCoarrays_DIR}/../../../bin")
    if (CAFRUN)
        set(Coarray_EXECUTABLE ${CAFRUN})
        message(STATUS "cafrun = ${CAFRUN}")
    else()
        message(FATAL_ERROR "cafrun not found")
    endif()
    include(ProcessorCount)
    ProcessorCount(Nproc)
    set(Coarray_MAX_NUMPROCS ${Nproc})
    set(Coarray_NUMPROC_FLAG -np)
    
    list(APPEND Coarray_REQUIRED_VARS ${Coarray_LIBRARY})
  elseif(CMAKE_Fortran_COMPILER_ID STREQUAL GNU)
    set(Coarray_COMPILE_OPTIONS -fcoarray=lib)
    list(APPEND Coarray_REQUIRED_VARS ${Coarray_COMPILE_OPTIONS})
  endif()

endif()

set(CMAKE_REQUIRED_FLAGS ${Coarray_COMPILE_OPTIONS})
set(CMAKE_REQUIRED_LIBRARIES ${Coarray_LIBRARY})
include(CheckFortranSourceCompiles)
check_fortran_source_compiles("program cs; real :: x[*]; end" f08coarray SRC_EXT f90)
unset(CMAKE_REQUIRED_FLAGS)
unset(CMAKE_REQUIRED_LIBRARIES)

list(APPEND Coarray_REQUIRED_VARS ${f08coarray})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Coarray
  REQUIRED_VARS Coarray_REQUIRED_VARS)

set(Coarray_LIBRARIES ${Coarray_LIBRARY})

mark_as_advanced(
  Coarray_LIBRARY
  Coarray_REQUIRED_VARS
)
