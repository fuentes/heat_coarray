program heat_1d
  use iso_c_binding
  use heat, only: diff2
  use parallel, only : tile, halo_exchange_1d, print_loc
  implicit none

  integer(c_int) ::  i, n = 10, is, ie, s(2), halo_width = 1, me
  real(c_double) , allocatable :: x_p(:)[:], x_l(:)[:]
  real(c_double) :: dt

  me = this_image()
  s = tile(n)
  is = s(1) - halo_width
  ie = s(2) + halo_width 
  allocate(x_p(is:ie)[*], x_l(is:ie)[*])
  sync all

  ! initialization 
  if (me == 1) then 
      x_l(is) = 1.d0
  else if (me == num_images()) then
      x_l(ie) = 1.d0
  end if 

  call halo_exchange_1d(n, x_l)
  x_p = x_l

  dt = 1.d-5

  TIME : do i=1, 1000
      if (mod(i, 100) == 0 ) then
         call print_loc( n, x_l )
      end if
      x_p =  x_l  + dt * diff2(x_l)
      x_l =  x_p
      call halo_exchange_1d(n, x_l)
  end do TIME

 end program heat_1d

