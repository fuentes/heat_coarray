module heat
  use iso_fortran_env
  use iso_c_binding
  use constants
  public :: diff2
  
contains

   function diff2(x) result(y)

      real(dp) , intent(in) :: x(:)
      real(dp) :: y(lbound(x,1):ubound(x, 1))
      integer :: n , p
      p =  lbound(x, 1)
      n =  ubound(x,1)
      y(p) = -2.d0 * x(p) + x(p+1)
      y(n) = -2.d0 * x(n) + x(n-1)
      y(p+1:n-1) = -2.d0 * x(p+1:n-1) + x(p:n-2) +x(p+2:n)

   end function diff2

end module heat
