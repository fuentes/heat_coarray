module coo_matrix_class
  use constants
  use sparse_matrix_class
  use collectives
  use tools
  use csr_matrix_class

  private
  public :: mat_vec

  type, extends(sparse_matrix), public :: coo_matrix
    integer :: k_0  = 0
    integer :: local_nrows = 0
    real(dp), allocatable :: vals(:)
    integer, allocatable :: i_vec(:)
    integer, allocatable :: j_vec(:)
    integer :: halo_size

  contains
    final :: delete_coo_matrix
    procedure :: mat_vec => coo_mat_vec
    procedure :: to_dense => coo_to_dense
  end type coo_matrix

  interface coo_matrix
    procedure new_coo_matrix
  end interface coo_matrix

contains

  type(coo_matrix) function new_coo_matrix(n, p, halo_size) result (coo)

    implicit none
    integer, intent(in) :: n, p
    integer, optional, intent(in) :: halo_size

    call init_sparse_matrix( coo, n, p , nnz=0)
    if (present( halo_size )) then
      coo % halo_size = halo_size
    else
      coo % halo_size = 0
    end if

  end function new_coo_matrix

  subroutine delete_coo_matrix(coo)

    type(coo_matrix), intent(inout) :: coo

    call coo % clean_sparse_matrix ()
    coo % halo_size = 0

    if (allocated( coo % vals )) deallocate( coo % vals)
    if (allocated( coo % i_vec )) deallocate( coo % i_vec)
    if (allocated( coo % j_vec )) deallocate( coo % j_vec)

  end subroutine delete_coo_matrix


  function coo_mat_vec(this, x) result (y)

    implicit none
    class(coo_matrix), intent(in) :: this
    real(dp), intent(in) :: x(:)
    real(dp), allocatable :: y(:), z(:)

    select type( this )
     class is( coo_matrix )
      stop "not yet implemented"
     class default
      stop  "wrong type used for coo_mat_vec"
    end select
  end function coo_mat_vec


  type(csr_matrix) function coo_to_csr(this)

    implicit none
    class(coo_matrix), intent(in) :: this

    stop  "not yet implemented "

  end function  coo_to_csr

  function coo_to_dense(this) result(a)

    implicit none
    class(coo_matrix), intent(in) :: this
    real(dp), allocatable :: a(:,:)

    stop  "not yet implemented "

  end function  coo_to_dense



end module coo_matrix_class
