module sparse_matrix_class
   
   use constants
   use abstract_matrix_class
   implicit none
   
   !> sparse matrix abstract type
   type, abstract, extends(abstract_matrix) :: sparse_matrix
       !> non zero number elements
       integer :: nnz 
   contains
       procedure (sparse_mat_vec), deferred :: mat_vec
       procedure (sparse_to_dense), deferred :: to_dense
       procedure :: clean_sparse_matrix
       procedure :: compute_partition
       generic :: operator(*) => mat_vec  
   end type sparse_matrix

   abstract interface
    
      !> sparse matrix - vector product \(y = A * x\)
      function sparse_mat_vec(this, x) result(y)
         use constants
         import :: sparse_matrix
         class(sparse_matrix), intent(in) :: this
         real(dp), intent(in)  :: x(:)
         real(dp), allocatable :: y(:)
      end function sparse_mat_vec

      !> converts to a dense matrix copied on all images (to do checks, do not scale)
      function sparse_to_dense(this) result(a)
         use constants
         import :: sparse_matrix      
         class(sparse_matrix), intent(in) :: this
         real(dp), allocatable :: a(:,:)
      end function sparse_to_dense

   end interface

contains 

     !> pseudo constructor because abstract types cannot be instantiated
     subroutine init_sparse_matrix(this, n, p, nnz)

      class(sparse_matrix), intent(inout) :: this
      integer, intent(in) :: n, p, nnz

      this % co_size = num_images()
      this % co_rank = this_image()
      this % global_nrows = n
      this % global_ncols = p
      this % nnz = nnz
  
    end subroutine init_sparse_matrix

    integer function compute_partition(this, global_size) result (local_size)

       class(sparse_matrix), intent(in) :: this
       integer, intent(in) :: global_size
       integer :: r, row_partition, k_0
       
       row_partition = this % co_size
       r = mod ( this % global_nrows, row_partition )
       local_size = ( this % global_nrows - 1 ) / row_partition + 1

       if ( r /= 0 .and. this % co_rank > r ) then
           local_size  = local_size - 1
       end if

    end function compute_partition

    !> clean sparse_matrix 
    subroutine clean_sparse_matrix(this)
      class(sparse_matrix), intent(inout) :: this

      this % co_size = 0
      this % co_rank = 0
      this % global_nrows = 0
      this % global_ncols = 0
      
    end subroutine clean_sparse_matrix
end module sparse_matrix_class
