module csr_matrix_class
    use constants
    use sparse_matrix_class
    use collectives
    use tools
    implicit none

    !> Compressed Sparse Row matrix type
    !> such as https://en.wikipedia.org/wiki/Sparse_matrix#Compressed_sparse_row_(CSR,_CRS_or_Yale_format)
    type, extends(sparse_matrix), public :: csr_matrix
        integer :: row_partition = 0
        integer :: local_nrows = 0
        integer :: local_nnz = 0
        real(dp), allocatable :: vals(:)
        integer, allocatable :: row_ptr(:)
        integer, allocatable :: col_val(:)
 
    contains
        final :: delete_csr_matrix
        procedure :: mat_vec => csr_mat_vec
        procedure :: to_dense => csr_to_dense
    end type csr_matrix
 
    interface csr_matrix
        procedure new_csr_matrix
    end interface csr_matrix

contains

    !> constructor
    type(csr_matrix) function new_csr_matrix(n, p, nnz) result (csr)
 
      integer, intent(in) :: n, p, nnz

      call init_sparse_matrix(csr, n, p, nnz)
      csr % local_nrows = csr % compute_partition( n ) 
 
    end function new_csr_matrix
 
    subroutine delete_csr_matrix(this)
 
      type(csr_matrix), intent(inout) :: this
 
      call this % clean_sparse_matrix ()
      this % row_partition = 0
      this % local_nrows = 0
      this % local_nnz = 0
 
      if (allocated( this % vals )) deallocate( this % vals)
      if (allocated( this % row_ptr )) deallocate( this % row_ptr )
      if (allocated( this % col_val )) deallocate( this % col_val )
 
    end subroutine delete_csr_matrix
 
 
    function csr_mat_vec(this, x) result (y)
 
        
        class(csr_matrix), intent(in) :: this
        real(dp), intent(in) :: x(:)
        real(dp), allocatable :: y(:), z(:)
        integer :: i, j, k
        
        select type( this )
           class is( csr_matrix )
           allocate( y(this % local_nrows) )
           y = 0.0_dp
           ! square case : is distributed across images
           if (size( x ) == this % local_nrows ) then
               z = gather_to_all( x ) 
           ! x a 
           else if (size( x ) == this % global_ncols ) then 
               z = x
           end if
           !! SpMV kernel is coded here 
           do j = 1, this % local_nrows
               do k = this % row_ptr(j) , this % row_ptr(j+1) - 1
                   y(j) = y(j) + this % vals(k) * z(this % col_val(k))
               end do
           end do
           class default
               print *, "wrong type used for csr_mat_vec"
               error stop
        end select
    end function csr_mat_vec
 
    function csr_to_dense(this) result(a)
 
        class(csr_matrix), intent(in) :: this
        real(dp), allocatable :: a(:,:), a_vec(:), a_loc(:,:) ! overkill method use a pointer here
        integer, allocatable :: nrows(:), c_nrows(:)
        integer :: i, j, local_nrows(1)
       
        allocate( a_loc(this % local_nrows, this % global_ncols) )
        
        do i = 1, this % local_nrows
            do j = this % row_ptr(i), this % row_ptr(i+1) - 1
                    a_loc(i, this % col_val(j)) = this % vals(j)
            end do
        end do    

        local_nrows = this % local_nrows
        nrows = gather_to_all(local_nrows)
        c_nrows = [0 , cumsum(nrows) ]

        associate (ncols => this % global_ncols)

            allocate( a(this % global_nrows, ncols) )
            a = 0.0_dp
            a_vec = gather_to_all(pack(a_loc, .true.))
            do i=1, num_images()
                a(c_nrows(i)+1:c_nrows(i+1), :) = reshape( a_vec(c_nrows(i)* ncols + 1:c_nrows(i+1)* ncols), &
                [c_nrows(i+1)-c_nrows(i), ncols] )
            end do
        
        end associate
    end function csr_to_dense


end module csr_matrix_class
