module solver
   use sparse_matrix_class
   use collectives
   use constants
   use io
   implicit none
   integer, parameter, private :: default_max_iterations = 100
   real(dp), parameter, private :: default_tolerance = 1.0e-9_dp

contains

    function cg(A, b, max_iterations, tolerance) result (x)

       class(sparse_matrix), intent(in) ::  A 
       real(dp), intent(in) :: b(:)

       integer, optional, intent(in) :: max_iterations
       real(dp), optional, intent(in) :: tolerance
       real(dp), allocatable :: x(:), r(:), p(:), v(:), disp(:)

       real(dp) :: alpha, beta, eps, gamma
       integer :: it, it_max, m

       if (present( max_iterations )) then
           it_max = max_iterations
       else
           it_max = default_max_iterations
       end if

       if (present( tolerance )) then
           eps = tolerance
       else
           eps = default_tolerance
       end if
       m = size( b )
       allocate(x(m), r(m), p(m), v(m), disp(m))
       x = 0.0_dp
       r = b
       p = r
       alpha = r .dot. r
       it = 0
       ITER : do it=1, it_max
          if (sqrt(alpha) < eps) exit ITER
          v = A *  p 
          beta = v .dot. p
          gamma = alpha / beta
          x = x + gamma * p
          r = r - gamma * v
          alpha = r .dot. r
          gamma = alpha / (gamma * beta)
          p = r + gamma * p
       end do ITER
    end function cg

end module solver
