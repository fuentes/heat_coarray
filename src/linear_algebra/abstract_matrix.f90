module abstract_matrix_class
    implicit none
    type, abstract, public :: abstract_matrix
        integer :: global_nrows
        integer :: global_ncols
        integer :: co_size 
        integer :: co_rank
    end type abstract_matrix

end module abstract_matrix_class
