module parallel
    use iso_c_binding
    public :: tile
contains

    function tile(n, id) result(s)
            integer, intent(in) :: n
            integer, optional :: id
            integer :: s(2)
            integer :: p, r, m, me
             
            p  = num_images()
            r  = mod( n, p )
            if ( present(id) ) then
               me = id
            else
               me = this_image()
            end if
            m  = (n - 1) / p + 1
            if (r /= 0 .and. me > r) then
               m = m - 1
               s(1) = (me - 1) * m + r + 1
            else
               s(1) = (me - 1) * m + 1
            end if
            s(2) = s(1) + m - 1
    end function tile

    subroutine halo_exchange_1d(n, x, width)  ! FIXME : use type to pass n
         integer(c_int), intent(in) :: n
         real(c_double), intent(inout), allocatable :: x(:)[:]
         integer(c_int), intent(in), optional :: width

         real(c_double), allocatable :: halo_r(:)[:], halo_l(:)[:]
         integer(c_int) :: s(2), me , halo_width

         if (present(width)) then 
             halo_width = width
         else
             halo_width = 1
         end if 
         
         ! computes bounds
         s = tile(n)
         me = this_image()
        
         allocate(halo_l(halo_width)[*])
         allocate(halo_r(halo_width)[*])

         halo_l = x(s(1) : s(1) + halo_width - 1)
         halo_r = x(s(2) - halo_width + 1 : s(2))

         sync all

         if (me >= 2) then
             x(s(1) - halo_width  : s(1) - 1) = halo_r(1:halo_width)[me - 1] 
         end if
         if (me < num_images()) then
            x(s(2) + 1 : s(2) + halo_width) = halo_l(1:halo_width)[me + 1] 
         end if
    
     end subroutine halo_exchange_1d

     subroutine print_loc(n, x)
         integer, intent(in) :: n
         real(c_double), intent(in), allocatable :: x(:)[:]
         integer :: s(2)
         character(len=1024) :: variable_format

         s = tile(n)
         print *, "(i0a" // spread("1x1f9.2",1,s(2)-s(1)+1)//")"
         !write (variable_format, *) "(i0a" // spread("1x1f9.2",1,s(2)-s(1)+1)//")"
         !print *, variable_format
         !print variable_format, this_image(), " => ", x(s(1):s(2))

    end subroutine print_loc
end module parallel
