module tools
    use constants
    implicit none

    public :: linspace, cumsum

    interface cumsum
        module procedure cumsum_real, cumsum_integer
    end interface

contains

    pure function linspace(a, b, N) result(x)
    
          real(dp) , intent(in) :: a, b
          integer , intent(in) :: N
          real(dp) :: x(N)
          integer  :: i
    
          x = [(i*(b-a)/(N-1)+a, i=0,N-1)]
    
    end function linspace


    pure function cumsum_real(x) result(y)
        implicit none
        real(dp), intent(in) :: x(:)
        real(dp) :: y(size(x,1))
        integer :: i
        y(1) = x(1)
        do i =2, size(x, 1)
           y(i) = y(i-1) + x(i)
        end do

    end function cumsum_real

    pure function cumsum_integer(x) result(y)
        implicit none
        integer, intent(in) :: x(:)
        integer :: y(size(x,1))
        integer :: i
        y(1) = x(1)
        do i =2, size(x, 1)
           y(i) = y(i-1) + x(i)
        end do

    end function cumsum_integer

end module tools
