module asserts

    use constants
    implicit none
    public :: assert_equals
 
    interface assert_equals

       module procedure assert_equals_d, assert_equals_c, assert_equals_vec, assert_equals_integer_vec

    end interface      


contains

    ! assert subroutine to test equality between reals
    subroutine  assert_equals_integer_vec(x, y)
        integer, intent(in) :: x(:), y(:)
        if (any((x /= y))) then
            print *, abs(x - y)
            stop -1
        end if

    end subroutine assert_equals_integer_vec

    ! assert subroutine to test equality between reals
    subroutine  assert_equals_d(x, y, prec)
        real(dp), intent(in) :: x, y, prec
        if (abs(x - y) > prec) then
            print *, abs(x - y)
            stop -1
        end if

    end subroutine assert_equals_d

    ! assert subroutine to test equality between complex
    subroutine  assert_equals_c(x, y, prec)
        complex(dp), intent(in) :: x, y
        real(dp), intent(in) :: prec
        if (abs(x - y) > prec) then
            print *, abs(x - y)
            stop -1
        endif

    end subroutine assert_equals_c

    subroutine assert_equals_vec(x, y, prec)
        real(dp), intent(in) :: x(:), y(:), prec
        if (sum(abs(x - y)) > prec) then 
           print *, abs(x - y) 
           stop -1
        end if
    
    end subroutine assert_equals_vec

end module asserts
