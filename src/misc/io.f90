module io
    use constants 
  
    public :: println

    interface println
        module procedure println_real_vec, println_real_mat
    end interface

contains

    subroutine println_real_vec(x) 
       real(dp) :: x(:)
       character(len=25) x_fmt
       write (x_fmt, '(a,i0,a)') "(", size(x,1) , "(1xf12.5)/)"
       print x_fmt, x 
    end subroutine println_real_vec

    subroutine println_real_mat(a) 
       real(dp) :: a(:,:)
       character(len=25) a_fmt
       write (a_fmt, '(a,i0,a,i0,a)') "(", size( a, 1 ), "(", size( a, 2), "1xf12.2/))"
       print a_fmt, a
    end subroutine println_real_mat

end module io
